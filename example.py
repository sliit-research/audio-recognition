import warnings
import json
warnings.filterwarnings("ignore")

from dejavu import Dejavu
from dejavu.recognize import FileRecognizer, MicrophoneRecognizer

# load config from a JSON file (or anything outputting a python dictionary)
with open("dejavu.cnf.SAMPLE") as f:
    config = json.load(f)

if __name__ == '__main__':

	# create a Dejavu instance
	djv = Dejavu(config)

	# Fingerprint all the wav's in the directory we give it - training module
	# djv.fingerprint_directory("wav", [".wav"])

	# Recognize audio from a file - return value array
	song = djv.recognize(FileRecognizer, "wav/ch02.wav")
	print "From file we recognized: %s\n" % song

	# Or recognize audio from your microphone for `secs` seconds
	
	secs = 5
	 song = djv.recognize(MicrophoneRecognizer, seconds=secs)
	 if song is None:
		print "Nothing recognized -- did you play the song out loud so your mic could hear it? :)"
	 else:
		print "From mic with %d seconds we recognized: %s\n" % (secs, song)

	Or use a recognizer without the shortcut, in anyway you would like

	# recognizer = FileRecognizer(djv)
	# song = recognizer.recognize_file("wav/ch02.wav")
	# print "No shortcut, we recognized: %s\n" % song